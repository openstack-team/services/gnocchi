gnocchi (4.6.4-7) unstable; urgency=medium

  * Re-upload source-only (last upload was a mistake).

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Feb 2025 09:30:26 +0100

gnocchi (4.6.4-6) unstable; urgency=medium

  * Blacklist broken test:
    - TestAggregatedTimeSerie.test_aggregation_std_with_unique
  * Restrict autopkgtest to amd64, arm64, ppc64el, riscv64.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Feb 2025 08:59:44 +0100

gnocchi (4.6.4-5) unstable; urgency=medium

  * Run tests at build time and in autopkgtest with MySQL (Closes: #1035131).

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Jan 2025 10:34:56 +0100

gnocchi (4.6.4-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090415).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 17:01:46 +0100

gnocchi (4.6.4-3) unstable; urgency=medium

  * Add change-default-number-of-sacks.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Nov 2024 09:50:56 +0100

gnocchi (4.6.4-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 17:08:39 +0200

gnocchi (4.6.4-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.
  * Removed patches applied upstream:
    - Remove_the_use_of_the_future_module.patch
    - setup-fix-usage-of-deprecated-get_script_header.patch

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 13:02:56 +0200

gnocchi (4.5.0-6) unstable; urgency=medium

  * Add close-on-exec{2,} = true in uwsgi config.
  * Remove extraneous python3-mock build dependency (Closes: #1071965).

 -- Thomas Goirand <zigo@debian.org>  Sun, 02 Jun 2024 12:51:06 +0200

gnocchi (4.5.0-5) unstable; urgency=medium

  * Add Remove_the_use_of_the_future_module.patch (Closes: #1059108).

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Dec 2023 12:19:32 +0100

gnocchi (4.5.0-4) unstable; urgency=medium

  * Add setup-fix-usage-of-deprecated-get_script_header.patch
    (Closes: #1042187).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Aug 2023 14:31:23 +0200

gnocchi (4.5.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 15:06:53 +0200

gnocchi (4.5.0-2) experimental; urgency=medium

  * Add missing backends = disable_by_file in api-past.ini.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 May 2023 17:22:24 +0200

gnocchi (4.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.
  * Rebased install-missing-files.patch.
  * Removed Support-later-versions-of-tenacity.patch.
  * Removed lsb-base depends.
  * Removed obsolete Breaks+Replaces: python-gnocchi.
  * debhelper-compat = 11.
  * Standards-Version: 4.6.1.
  * Build-depends on openstack-pkg-tools (>= 123~).

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Apr 2023 16:27:02 +0200

gnocchi (4.4.2-1) unstable; urgency=medium

  * New upstream release.
  * Switch to stestr.
  * Add Support-later-versions-of-tenacity.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Feb 2023 13:34:02 +0100

gnocchi (4.4.0-3) unstable; urgency=medium

  * Add disable_by_file_path.patch

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Nov 2022 17:26:09 +0100

gnocchi (4.4.0-2) unstable; urgency=medium

  * Tune gnocchi-api-uwsgi.ini for performance.
  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Mar 2021 13:32:56 +0100

gnocchi (4.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.
  * Removed py3-compat.patch.
  * Add a debian/README.Debian telling the package cannot be used with SQLite.
  * Add add-header = Connection: close in gnochi-api-uwsgi.ini.
  * Configure with threads = 32 by default (as per Gnocchi's doc).
  * Add listen = 100 in the default UWSGI config.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Oct 2020 23:04:47 +0200

gnocchi (4.3.4-3) unstable; urgency=medium

  * Rebuild source-only.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Nov 2019 21:03:26 +0100

gnocchi (4.3.4-2) unstable; urgency=medium

  * Rebuilt source-only.

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 01 Nov 2019 14:44:21 +0100

gnocchi (4.3.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.

  [ Michal Arbet ]
  * New upstream version

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 09 Sep 2019 13:19:28 +0200

gnocchi (4.3.1-4) unstable; urgency=medium

  [ Marcin Juszkiewicz ]
  * Updated upstream URL in control/copyright/watch.

  [ Michal Arbet ]
  * d/control:
      - Bump openstack-pkg-tools to version 99
      - Add me to uploaders field
  * d/copyright: Add me to copyright file
  * d/rules: Remove python3 from oslo-config-generator

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 03 May 2019 17:39:11 +0200

gnocchi (4.3.1-3) unstable; urgency=medium

  * Add a gnocchi logrotate file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Nov 2018 14:37:07 +0100

gnocchi (4.3.1-2) unstable; urgency=medium

  * Uploading to unstable:
    - This makes the build reproducible (Closes: #892419).
    - Fix FTBFS (Closes: #911405).
  * Fix gnocchi-statsd python3 depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Nov 2018 13:10:39 +0000

gnocchi (4.3.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Refreshed patches.
  * export SETUPTOOLS_SCM_PRETEND_VERSION=.
  * Do not call setup.py clean.
  * Add install-missing-files.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 25 Aug 2018 15:18:34 +0200

gnocchi (4.2.0-5) unstable; urgency=medium

  * Switch gnocchi to openstack-pkg-tools >= 81~ style of uwsgi app.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Jun 2018 15:50:00 +0200

gnocchi (4.2.0-4) unstable; urgency=medium

  * Add patch to remove dependency on distutils (Closes: #896594).

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 Jun 2018 10:41:46 +0200

gnocchi (4.2.0-3) unstable; urgency=medium

  * Add missing gnocchi-statsd daemon package and init script.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 May 2018 09:23:50 +0200

gnocchi (4.2.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Fixed dbc postrm.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Mar 2018 16:43:11 +0000

gnocchi (4.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.
  * Add patch for Py3 compat, courtesy of James Page from Canonical.
  * Blacklist test_gnocchi_config_generator_run(), we don't care about it
    anyway, as we're using oslo-config-generator.
  * Fixed uwsgi params.
  * Dropped debconf templates, using openstack-pkg-tools >= 70~.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Feb 2018 13:12:12 +0000

gnocchi (4.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Fixed gnocchi-api UWSGI startup.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Feb 2018 14:25:47 +0100

gnocchi (4.0.3-3) unstable; urgency=medium

  * python3-gnocchi breaks+replaces python-gnocchi, therefore allowing
    upgrades from older Gnocchi that was using Python 2.7 (Closes: #881247).
  * Fixed encoding of pt.po.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Nov 2017 22:40:08 +0000

gnocchi (4.0.3-2) unstable; urgency=medium

  * Fixed version of python-sqlalchemy-utils (>= 0.32.14).
  * Updated pt.po (Closes: #876172).
  * Running gnocchi-upgrade instead of dbsync (Closes: #853121).
  * Uploading to unstable.
  * Add missing build-depends: python-all, python-pbr, python-setuptools,
    python3-testresources.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Nov 2017 11:31:24 +0000

gnocchi (4.0.3-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Standards-Version is now 4.1.1.
  * Switch the package to Python 3 only, drop Python 2.
  * Add debian/bin with gnocchi-config-generator entrypoint.
  * Fixed oslo-config-generator namespace list for this release.
  * Fixed source location for policy.json and api-paste.ini.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Oct 2017 06:33:31 +0200

gnocchi (3.0.4-4) unstable; urgency=medium

  * German debconf translation update (Closes: #842481).

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Apr 2017 18:13:32 +0200

gnocchi (3.0.4-3) unstable; urgency=medium

  * Team upload.
  * Revert net-tools dependency.
  * Bump build dependency on openstack-pkg-tools (Closes: #858697).

 -- David Rabel <david.rabel@noresoft.com>  Sat, 01 Apr 2017 11:35:30 +0200

gnocchi (3.0.4-2) unstable; urgency=medium

  * Team upload.
  * Add missing dependency net-tools (Closes: #858697)

 -- David Rabel <david.rabel@noresoft.com>  Sat, 25 Mar 2017 12:06:03 +0100

gnocchi (3.0.4-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Bumped debhelper compat version to 10
  * Added lsb-base to depends

  [ Ondřej Kobližek ]
  * New upstream release (Closes: #852991)

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 02 Feb 2017 14:44:31 +0100

gnocchi (3.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/s/options: extend-diff-ignore of .gitreview

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Debconf translation:
    - it (Closes: #839198).

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Oct 2016 09:14:26 +0200

gnocchi (3.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using OpenStack's Gerrit as VCS URLs.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2016 16:38:32 +0200

gnocchi (2.0.2-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Fixed upstream URL

  [ Thomas Goirand ]
  * Updated Danish translation of the debconf templates (Closes: #830650).
  * Now using gnocchi-upgrade, and not gnocchi-dbsync (Closes: #832792).

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Aug 2016 12:21:06 +0000

gnocchi (2.0.2-6) unstable; urgency=medium

  * Fixed section to be net instead of python (Closes: #825364).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jun 2016 07:16:10 +0000

gnocchi (2.0.2-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Removed UPSTREAM_GIT with default value
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * Added Japanese debconf templates translation update (Closes: #820769).
  * Updated Dutch debconf templates (Closes: #823439).
  * Updated Brazilian Portuguese debconf templates (Closes: #824292).

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Apr 2016 18:54:09 +0200

gnocchi (2.0.2-4) unstable; urgency=medium

  * Uploading to unstable.
  * Updated ja.po debconf translation (closes: #819135).

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2016 11:02:48 +0200

gnocchi (2.0.2-3) experimental; urgency=medium

  * Added missing (build-)depends: python-lz4.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 13:29:28 +0000

gnocchi (2.0.2-2) experimental; urgency=medium

  * Do not use Keystone admin auth token to register API endpoints.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 13:10:49 +0000

gnocchi (2.0.2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Rename again GNOCCHI_INDEXER_URL env var (upstream constantly changes it).
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2016 14:05:32 +0100

gnocchi (1.3.4-1) experimental; urgency=medium

  * New upstream release.
  * Clean /var/{lib,log}/gnocchi on purge (Closes: #810700).
  * Added nl.po debconf translation (Closes: #812356).
  * Fixed running tests with pgsql (ie: GNOCCHI_TEST_INDEXER_URL env var).
  * HTTPS VCS URLs.

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Feb 2016 09:15:03 +0000

gnocchi (1.3.3+2016.01.27.git.e1339d77a9-1) unstable; urgency=medium

  * New upstream release based on commit e1339d77a9.
  * Removed influxdb and influxdb-dev from build-depends-indep.
  * Do not build-depends on python-ceilometer.
  * Fixed python-future version to be >= 0.15.
  * Fixed (build-)depends for this release.
  * Pushes app.wsgi to /usr/share/gnocchi-common.
  * Fixed debian/copyright ordering.
  * Follow upstream rename of GNOCCHI_TEST_INDEXER_URL to GNOCCHI_INDEXER_URL.
  * Fixed namespaces when generating config file.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Jan 2016 04:13:02 +0000

gnocchi (1.3.0-4) unstable; urgency=medium

  * French debconf templates translation update (Closes: #806506).

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Dec 2015 10:21:02 +0100

gnocchi (1.3.0-3) unstable; urgency=medium

  * Added missing dbconfig-common dependency (Closes: #806538).

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Dec 2015 08:22:21 +0000

gnocchi (1.3.0-2) unstable; urgency=medium

  * Adds missing config file: api-paste.ini.
  * Fixed auth_protocol to be http by default.
  * Added debconf translations imported from Glance.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Nov 2015 13:18:12 +0000

gnocchi (1.3.0-1) unstable; urgency=medium

  * Initial release. (Closes: #799374)

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Mar 2015 10:32:47 +0100
