#!/usr/bin/make -f

UPSTREAM_GIT:=https://github.com/gnocchixyz/gnocchi
include /usr/share/openstack-pkg-tools/pkgos.make

export SETUPTOOLS_SCM_PRETEND_VERSION=$(shell dpkg-parsechangelog -SVersion | sed -e 's/^[[:digit:]]*://' -e 's/[-].*//' -e 's/~/.0/' -e 's/+dfsg1//' | head -n 1)

UNIT_TEST_BLACKLIST = test_carbonara.CarbonaraCmd.*|.*test_bin\.BinTestCase\.test_gnocchi_config_generator_run.*

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

override_dh_clean:
	dh_clean
	rm -rf build debian/gnocchi-common.postinst debian/gnocchi-common.config debian/gnocchi-api.config debian/gnocchi-api.postinst
	rm -rf debian/CHANGEME-common.postrm debian/*.templates debian/po

override_dh_auto_clean:
	echo "Do nothing ..."

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func gnocchi-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func gnocchi-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func gnocchi-api.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func gnocchi-api.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm gnocchi-common.postrm
	pkgos-merge-templates gnocchi-api gnocchi endpoint
	pkgos-merge-templates gnocchi-common gnocchi db ksat

override_dh_auto_install:
	echo "Do nothing..."

override_dh_auto_test:
	echo "Do nothing..."

override_dh_install:
	set -e ; for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp; \
	done

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	./debian/run_tests.sh
#	echo "Not running unit tests until launching mysql or postgresql is fixed..."
#	@echo "===> Running tests"
#	set -e ; set -x ; for i in $(PYTHON3S) ; do \
#		PYMAJOR=`echo $$i | cut -d'.' -f1` ; \
#	BINDIR=`pg_config --bindir` ; \
#	PG_MYTMPDIR=`mktemp -d` ; \
#	export LC_ALL="C" ; \
#	export LANGUAGE=C ; \
#	PGSQL_PORT=9823 ; \
#	$$BINDIR/initdb -D $$PG_MYTMPDIR ; \
#	$$BINDIR/pg_ctl -w -D $$PG_MYTMPDIR -o "-k $$PG_MYTMPDIR -p $$PGSQL_PORT" start > /dev/null ; \
#	attempts=0 ; \
#	while ! [ -e $$PG_MYTMPDIR/postmaster.pid ] ; do \
#		if [ $$attempts -gt 10 ] ; then \
#			echo "Exiting test: postgres pid file was not created after 30 seconds" ; \
#			exit 1 ; \
#		fi ; \
#		attempts=$$((attempts+1)) ; \
#		sleep 3 ; \
#	done ; \
#		export GNOCCHI_INDEXER_URL="postgresql:///template1?host=$$PG_MYTMPDIR&port=9823" ; \
#		export GNOCCHI_TEST_STORAGE_DRIVER=file ; \
#		echo "===> Testing with python$$i (python$$PYMAJOR)" ; \
#		rm -rf .testrepository ; \
#		testr-python$$PYMAJOR init ; \
#		TEMP_REZ=`mktemp -t` ; \
#		export PATH=$(PATH):$(CURDIR)/debian/bin && PYTHONPATH=$(CURDIR):$(CURDIR)/debian/bin PYTHON=python$$i testr-python$$PYMAJOR run --subunit 'gnocchi\.tests\.(?!.*('"$(UNIT_TEST_BLACKLIST)"'))' | tee $$TEMP_REZ | subunit2pyunit ; \
#		cat $$TEMP_REZ | subunit-filter -s --no-passthrough | subunit-stats ; \
#		rm -f $$TEMP_REZ ; \
#		testr-python$$PYMAJOR slowest ; \
#		echo "===> Stopping PGSQL" ; \
#		$$BINDIR/pg_ctl stop -D $$PG_MYTMPDIR ; \
#	done
endif


	mkdir -p $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/gnocchi/indexer
	cp -auxf gnocchi/indexer/alembic $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/gnocchi/indexer
	rm -rf $(CURDIR)/debian/python*-gnocchi/usr/lib/python*/dist-packages/*.pth
	rm -rf $(CURDIR)/debian/python*-gnocchi/usr/etc

	mkdir -p $(CURDIR)/debian/gnocchi-common/usr/share/gnocchi-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/gnocchi-common/usr/share/gnocchi-common/gnocchi.conf \
		--wrap-width 140 \
		--namespace gnocchi \
		--namespace oslo.db \
		--namespace oslo.middleware.cors \
		--namespace oslo.middleware.healthcheck \
		--namespace oslo.middleware.http_proxy_to_wsgi \
		--namespace oslo.policy \
		--namespace cotyledon \
		--namespace keystonemiddleware.auth_token
	sed -i 's|^[ \t#]*url[ \t#]*=.*|url = sqlite:////var/lib/gnocchi/gnocchidb|' $(CURDIR)/debian/gnocchi-common/usr/share/gnocchi-common/gnocchi.conf

	dh_install
	dh_missing --fail-missing


override_dh_sphinxdoc:
#		echo "===> Starting PGSQL" ; \
#		BINDIR=`pg_config --bindir` ; \
#		PG_MYTMPDIR=`mktemp -d` ; \
#		chown postgres:postgres $$PG_MYTMPDIR || true ; \
#		export PGHOST=$$PG_MYTMPDIR ; \
#		chmod +x debian/start_pg.sh ; \
#		debian/start_pg.sh $$PG_MYTMPDIR ; \
#		export GNOCCHI_TEST_INDEXER_URL="postgresql:///template1?host=$$PG_MYTMPDIR&port=9823" ; \
#	PYTHONPATH=. sphinx-build -b html doc/source debian/python3-gnocchi-doc/usr/share/doc/python3-gnocchi-doc/html ; \
#	dh_sphinxdoc ; \
#		$$BINDIR/pg_ctl stop -D $$PG_MYTMPDIR
	echo "Do nothing..."

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
