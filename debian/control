Source: gnocchi
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools (>= 123~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx,
Build-Depends-Indep:
 alembic,
 libpq-dev,
 mariadb-server,
 postgresql,
 postgresql-server-dev-all,
 python3-boto3,
 python3-botocore,
 python3-cachetools,
 python3-cotyledon,
 python3-coverage,
 python3-daiquiri,
 python3-doc8,
 python3-fixtures,
 python3-gabbi,
 python3-iso8601,
 python3-jsonpatch,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-lz4,
 python3-monotonic,
 python3-mysqldb,
 python3-numpy,
 python3-os-testr,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-paste,
 python3-pastedeploy,
 python3-pecan,
 python3-prettytable,
 python3-protobuf,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing,
 python3-pytimeparse,
 python3-qpid-proton,
 python3-redis,
 python3-snappy,
 python3-sphinx-bootstrap-theme,
 python3-sphinx-rtd-theme,
 python3-sphinxcontrib.httpdomain,
 python3-sqlalchemy,
 python3-sqlalchemy-utils,
 python3-stestr,
 python3-stevedore,
 python3-swiftclient,
 python3-sysv-ipc,
 python3-tenacity,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-tooz,
 python3-ujson,
 python3-voluptuous,
 python3-webob,
 python3-webtest,
 python3-werkzeug,
 python3-wsgi-intercept,
 python3-yaml,
 subunit,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/gnocchi
Vcs-Git: https://salsa.debian.org/openstack-team/services/gnocchi.git
Homepage: https://gnocchi.xyz/

Package: gnocchi-api
Architecture: all
Depends:
 adduser,
 debconf,
 gnocchi-common (= ${binary:Version}),
 python3-keystoneclient,
 python3-openstackclient,
 python3-pastescript,
 q-text-as-data,
 uwsgi-plugin-python3,
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - API daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the API server.

Package: gnocchi-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 debconf,
 python3-gnocchi (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - common files
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the common files.

Package: gnocchi-metricd
Architecture: all
Depends:
 gnocchi-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - metric daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the metric daemon.

Package: gnocchi-statsd
Architecture: all
Depends:
 gnocchi-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - statsd daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the statsd daemon.

Package: python3-gnocchi
Section: python
Architecture: all
Depends:
 alembic,
 python3-boto3,
 python3-botocore,
 python3-cachetools,
 python3-cotyledon,
 python3-daiquiri,
 python3-iso8601,
 python3-jsonpatch,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-lz4,
 python3-monotonic,
 python3-numpy,
 python3-oslo.config,
 python3-oslo.db,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-oslosphinx,
 python3-paste,
 python3-pastedeploy,
 python3-pbr,
 python3-pecan,
 python3-prettytable,
 python3-protobuf,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing,
 python3-pytimeparse,
 python3-redis,
 python3-snappy,
 python3-sqlalchemy,
 python3-sqlalchemy-utils,
 python3-stevedore,
 python3-swiftclient,
 python3-tenacity,
 python3-tooz,
 python3-ujson,
 python3-voluptuous,
 python3-webob,
 python3-werkzeug,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 gnocchi-doc,
Description: Metric as a Service - Python 3.x
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the Python 3.x module.
